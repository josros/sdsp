# Software development success principles

(SCRUM aligned but not limited)

1. Larger products are composed of smaller products.
1. The scope of smaller products is aligned with bounded contexts
1. Even smaller products represent verticals and do have e2e character
1. At the maximum a small product is as big as it can be developed by a single product team.
1. A single product team consists of a single product owner and at a max 4-5 people (including developers, SCRUM Master and other potential roles)
1. The organisation behind larger products is aligned with SCRUM@scale (or a similar approach)
1. All product decisions are made by the product team
1. Timelines regarding the product development are made by the product team
1. The largest functional unit is called epic
1. Epics are in general larger than a single sprint but small enough to keep track of
1. The content of an epic is user centered and breaks the user interfaces down to a technical api
1. Epics are planned by the product team
1. Every member of the product team agrees on the content of an epic
1. Completely planned epics are the foundation for deriving stories
1. A single story always belongs to a single epic
1. All stories assigned to an epic describe the total extent of an epic
1. At the maximum a story is a big as it can be processed in a single sprint
1. Essentially stories consist of acceptance criteria written in the form of given, when, then
1. Stories to be processed have a clear functional scope and a reliable estimation
1. The estimation of a single epic is the accumulated estimation of all stories assigned
1. Reliable timelines can only be derived from estimated epics considering prioritization and the dependency between stories
1. The product owner prioritizes epics and stories
1. The main focus of the product team is the user
1. Ideally product decisions are justified based on the users behavior
1. Architectural decisions are made by the product team
1. Technology decisions are made by the product team
1. The product team decides on required roles and the teams composition
1. The product team takes over the recruiting of new team members
1. The product team challenges its progress consistently in front of stakeholders
1. Stakeholders give feedback but aren't allowed to make decisions regarding the product teams responsibility
1. Avoid an artificial hierarchy within the product team (e.g. if working with service providers, they may not contradict an internal product owner)
1. Ideally developers can take responsibility for all parts of the system (e.g. avoid frontiers between backend and frontend)
1. Put the most effort and the best resources into core domains
